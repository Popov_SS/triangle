def is_triangle(a, b, c):
    '''
    please add your solution here or call your solution implemented in different function from here  
    then change return value from 'False' to value that will be returned by your solution
    '''
    # a triangle can be costructed when a sum of any 2 sides of the triangle is greater than the third side
    # so all I need to do is write an "if" statement
    if (a + b <= c) or (a + c <= b) or (b + c <= a): 
        return False
    else: 
        return True   
